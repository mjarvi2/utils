#!/bin/bash

# Step 1: Create the virtual environment
python3 -m venv .venv

# Step 2: Get the current directory
dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Define paths to Python and pip binaries in the virtual environment
pythonPath="$dir/.venv/bin/python3"
pipPath="$dir/.venv/bin/pip"

# Step 3: Install the pip requirements from the requirements.txt file
$pipPath install -r "$dir/requirements.txt"

# Step 4: Update the shebang line in the 'create-users.py' script
# Note: Ensure that 'create-users.py' is in the current directory or adjust the path accordingly

# Check if the script file exists
scriptFile="$dir/create-users.py"
if [ -f "$scriptFile" ]; then
    # Use 'sed' to replace the existing shebang line with the new path to the Python binary
    # '-i' option edits files in place (i.e., saves the changes to the file directly)
    # '^#!.*' regex matches any line starting with shebang
    # '!b' means "skip to end if line starts with '!'"
    # 's' command replaces the matched line with new shebang line
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS
        sed -i '' "1s|^#!.*|#!$pythonPath|" "$scriptFile"
    else
        # Linux and others
        sed -i "1s|^#!.*|#!$pythonPath|" "$scriptFile"
    fi
else
    echo "Error: Script file $scriptFile does not exist."
    exit 1
fi

echo "Environment setup and script modification complete."

