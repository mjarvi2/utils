#!/home/groot/repos/utils/user-password-generator/.venv/bin/python3

import os
import sys
import names
import random
import pandas as pd
import json

FIRST_NAME_CHARS_IN_USERNAME=3
PASSWORD_FILE="passwords.txt"
USERS_FILE="users.csv"

def get_rand_first_last():
    first = names.get_first_name()
    last = names.get_last_name()
    return first, last

def generate_users_first_last(num_of_users):
    print(f"Generating {num_of_users} users")
    users = {}
    for id in range(num_of_users):
        uid=id+1
        first, last = get_rand_first_last()
        first_lower=first.lower()
        last_lower=last.lower()
        first_name_username=first_lower[:FIRST_NAME_CHARS_IN_USERNAME]
        username=f"{first_name_username}{last_lower}"
        users[uid]={}
        users[uid]["First Name"]=first
        users[uid]["Last Name"]=last
        users[uid]["username"]=username
    return users
        
def pick_password():
    # This implementation is designed to handle very large password files
    random_line = None
    filename=PASSWORD_FILE
    with open(filename, 'r') as file:
        for i, line in enumerate(file, start=1):
            if random.randrange(i) == 0:
                random_line = line.strip()
    return random_line

def add_password_to_user(users):
    for user in users:
        password=pick_password()
        username=users[user]["username"]
        users[user]["password"]=password
        users[user]["username:password"]=f"{username}:{password}"
    return users

def generate_csv(users):
    df = pd.DataFrame.from_dict(users, orient='index')
    df.to_csv(USERS_FILE, index=False)

def csv_to_json():
    df = pd.read_csv(USERS_FILE)
    df.index = df.index + 1
    json_data = df.to_json(orient='index', indent=4)
    data_dict = json.loads(json_data)
    users = {int(k): v for k, v in data_dict.items()}
    return users

def generate_users(num_of_users):
    users=generate_users_first_last(num_of_users)
    users_with_passwords=add_password_to_user(users)
    generate_csv(users_with_passwords)

def change_passwords():
    users=csv_to_json()
    users_updated=add_password_to_user(users)
    generate_csv(users_updated)

if __name__ == "__main__":
    self_script_name = sys.argv[0]
    if len(sys.argv) != 2 and not os.path.isfile(USERS_FILE):
        print(f"Usage when users file does not exist: {self_script_name} <number_of_users>")
        sys.exit(1)
    elif len(sys.argv) == 2 and not os.path.isfile(USERS_FILE):
        try:
            num_of_users=int(sys.argv[1])
        except:
            print(f"{sys.argv[1]} is not a number")
            exit(1)
        generate_users(num_of_users)
    elif os.path.isfile(USERS_FILE):
        change_passwords()
    else:
        print(f"Usage when {USERS_FILE} does not exist: ./{self_script_name} <numer_of_users>")
        print(f"Usage when {USERS_FILE} does exist, to change passwords: ./{self_script_name}")