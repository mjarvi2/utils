import argparse
from PIL import Image, ImageOps

def invert_image(input_file, output_file):
    # Open the image file
    image = Image.open(input_file)
    
    # Print the original mode for debugging
    print(f"Original image mode: {image.mode}")

    # Handle images with an alpha channel separately
    if image.mode == 'RGBA':
        # Split the image into its respective bands
        r, g, b, a = image.split()
        
        # Create an RGB image with the inverted colors
        rgb_image = Image.merge('RGB', (r, g, b))
        inverted_rgb_image = ImageOps.invert(rgb_image)
        
        # Split the inverted image back into its components
        r_inv, g_inv, b_inv = inverted_rgb_image.split()
        
        # Merge the inverted RGB channels back with the original alpha channel
        inverted_image = Image.merge('RGBA', (r_inv, g_inv, b_inv, a))
    elif image.mode == 'LA':
        # Split the image into its respective bands
        l, a = image.split()
        
        # Invert the L channel
        inverted_l = ImageOps.invert(l)
        
        # Merge the inverted L channel back with the original alpha channel
        inverted_image = Image.merge('LA', (inverted_l, a))
    else:
        # Ensure the image is in a mode that can be inverted
        if image.mode not in ('RGB', 'L'):
            image = image.convert('RGB')
        
        # Invert the image colors
        inverted_image = ImageOps.invert(image)

    # Save the inverted image
    inverted_image.save(output_file)

if __name__ == "__main__":
    # Set up argument parser
    parser = argparse.ArgumentParser(description='Invert the colors of a PNG image.')
    parser.add_argument('input_file', type=str, help='The input PNG file')
    parser.add_argument('output_file', type=str, help='The output PNG file')

    # Parse arguments
    args = parser.parse_args()

    # Invert the image
    invert_image(args.input_file, args.output_file)


