#!/bin/bash

set -e

# Install required packages if not available
if ! command -v htpasswd &> /dev/null; then
    echo "Installing apache2-utils..."
    sudo apt update && sudo apt install -y apache2-utils
fi

if ! command -v openssl &> /dev/null; then
    echo "Installing OpenSSL..."
    sudo apt install -y openssl
fi

# Ask for username and password
read -p "Enter a username for Prometheus authentication: " USERNAME
read -sp "Enter a password for $USERNAME: " PASSWORD
echo

# Create or update the .htpasswd file
htpasswd -bc ./htpasswd "$USERNAME" "$PASSWORD"
echo "Created .htpasswd file successfully."

# Generate SSL certificate
SSL_DIR="./certs"
mkdir -p "$SSL_DIR"

echo "Generating a self-signed SSL certificate..."
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout "$SSL_DIR/nginx-selfsigned.key" \
    -out "$SSL_DIR/nginx-selfsigned.crt" \
    -subj "/C=US/ST=State/L=City/O=Organization/CN=localhost"

echo "SSL certificate and key generated successfully."

# Start the Docker services
echo "Starting Prometheus, Grafana, and NGINX..."
docker-compose up -d

echo "Setup complete. Access Prometheus at: https://localhost:9090"
echo "Note: You may need to accept the self-signed certificate warning in your browser."

