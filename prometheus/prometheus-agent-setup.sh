#!/bin/bash

PROMETHEUS_IP="172.16.223.96"

PROMETHEUS_COMPOSE_PATH="/opt/prometheus/docker-compose.yml"

DOCKER_PROXY_CONTENTS="[Service]
Environment='HTTP_PROXY=http://zathras:password1!@172.16.0.1:3128'
Environment='HTTPS_PROXY=http://zathras:password1!@172.16.0.1:3128'"

PROMETHUS_EXPORTER_CONFIG="services:
  node_exporter:
    image: quay.io/prometheus/node-exporter:latest
    container_name: node_exporter
    command:
      - '--path.rootfs=/host'
    network_mode: host
    pid: host
    restart: unless-stopped
    volumes:
      - '/:/host:ro,rslave'"

if  [ $UID -ne 0 ]
then
    echo "[ERROR] You must run this script as root!"
    exit
fi

echo "Checking pre-reqs..."

if systemctl is-active --quiet docker; then
    echo "Docker is running."
else
    echo "Docker is not running. Starting Docker..."

    # Start Docker
    sudo systemctl start docker

    if [ $? -eq 0 ]; then
        echo "Docker has been started successfully."
    else
        echo "Error: Failed to start Docker. Please check your Docker installation."
        exit 1
    fi
fi

# Check if Docker Compose is installed
if command -v docker-compose &> /dev/null; then
    echo "Docker Compose is installed."
else
    echo "Docker Compose is not installed. Installing Docker Compose..."

    # Install Docker Compose
    sudo apt update
    sudo apt install -y docker-compose

    if [ $? -eq 0 ]; then
        echo "Docker Compose has been installed successfully."
    else
        echo "Error: Failed to install Docker Compose. Please check your internet connection and try again."
        exit 1
    fi
fi

proxy_conf_path="/etc/systemd/system/docker.service.d/http-proxy.conf"

if [ -f "$proxy_conf_path" ]; then
    DOCKER_PROXY_CONTENTS=$(cat "$proxy_conf_path")
    if [ "$DOCKER_PROXY_CONTENTS" != "$DOCKER_PROXY_CONTENTS" ]; then
        echo "Updating proxy configuration in $proxy_conf_path..."
        echo "$DOCKER_PROXY_CONTENTS" | sudo tee "$proxy_conf_path" > /dev/null
        sudo systemctl daemon-reload
        sudo systemctl restart docker
        echo "Proxy configuration updated and Docker daemon restarted."
    else
        echo "Proxy configuration in $proxy_conf_path is correct."
    fi
else
    echo "Creating directory and proxy configuration in $proxy_conf_path..."
    sudo mkdir -p "/etc/systemd/system/docker.service.d"
    echo "$DOCKER_PROXY_CONTENTS" | sudo tee "$proxy_conf_path" > /dev/null
    sudo systemctl daemon-reload
    sudo systemctl restart docker
    echo "Directory and proxy configuration created, and Docker daemon restarted."
fi

echo "pre checks were successful!\nInstalling prometheus"

if [ -f "$PROMETHEUS_COMPOSE_PATH" ]; then
    current_prometheus_compose_contents=$(cat "$PROMETHEUS_COMPOSE_PATH")
    if [ "$current_prometheus_compose_contents" != "$PROMETHUS_EXPORTER_CONFIG" ]; then
        echo "Updating Prometheus Node Exporter configuration in $PROMETHEUS_COMPOSE_PATH..."
        echo "$PROMETHUS_EXPORTER_CONFIG" | sudo tee "$PROMETHEUS_COMPOSE_PATH" > /dev/null
    else
        echo "Prometheus Node Exporter configuration in $PROMETHEUS_COMPOSE_PATH is correct."
    fi
else
    echo "Creating Prometheus Node Exporter configuration in $PROMETHEUS_COMPOSE_PATH..."
    mkdir -p "/opt/prometheus"
    echo "$PROMETHUS_EXPORTER_CONFIG" | sudo tee "$PROMETHEUS_COMPOSE_PATH" > /dev/null
fi

# Start Prometheus Node Exporter container
echo "Starting Prometheus Node Exporter container..."
sudo docker-compose -f "$PROMETHEUS_COMPOSE_PATH" up -d
prometheus_ufw_rule="allow from "$PROMETHEUS_IP" to any port 9100"
existing_rule=$(ufw show added | grep "$prometheus_ufw_rule")

if [ -z "$existing_rule" ]; then
    # Rule not found or has changed, add or update it
    echo "Adding or updating UFW rule for Prometheus Node Exporter..."
    ufw allow from "$PROMETHEUS_IP" to any port 9100

    # Reload UFW
    sudo ufw reload

    echo "UFW rule added or updated and UFW reloaded."
else
    echo "UFW rule for Prometheus Node Exporter already exists and is correct."
fi

localhost_ipv4=$(hostname -I | awk '{print $1}')
echo "Add This IP to Prometheus: $localhost_ipv4"
