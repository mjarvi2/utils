#!/bin/bash

PROXY_IP="172.16.1.3"
LOCALHOST_IP=$(hostname -I | awk '{print $1}')

if  [ $UID -ne 0 ]
then
    echo "[ERROR] You must run this script as root!"
    exit
fi

if [ -z "$1" ]; then
    echo "Usage: $0 <IP_ADDRESS>"
    exit 1
fi

ip_address="$1"
prometheus_config="/home/kaiju/Prometheus/prometheus.yml"

if [ ! -f "$prometheus_config" ]; then
    echo "Prometheus configuration file not found at $prometheus_config."
    exit 1
fi

# Check if the IP address is already present in the configuration
if grep -q "$ip_address:9100" "$prometheus_config"; then
    echo "$ip_address is already present in the Prometheus configuration."
    exit 1
fi

# Add the IP address to the configuration
echo "Adding $ip_address to Prometheus configuration..."
echo "        - '$ip_address:9100'" | sudo tee -a "$prometheus_config" > /dev/null

echo "IP address $ip_address added to Prometheus configuration."

echo "reloading prometheus config"
curl -X POST localhost:9090/-/reload


grafana_ufw_rule="allow from $PROXY_IP to any port 3000"
existing_rule=$(ufw show added | grep "$grafana_ufw_rule")

if [ -z "$existing_rule" ]; then
    # Rule not found or has changed, add or update it
    echo "Adding or updating UFW rule for Grafana..."
    ufw  allow from $PROXY_IP to any port 3000

    # Reload UFW
    sudo ufw reload

    echo "UFW rule added or updated and UFW reloaded."
else
    echo "UFW rule for Grafana"
fi

prometheus_ufw_rule="allow from $LOCALHOST_IP to any port 9090"
existing_rule=$(ufw show added | grep "$ssh_ufw_rule")

if [ -z "$existing_rule" ]; then
    # Rule not found or has changed, add or update it
    echo "Adding or updating UFW rule for Prometheus Node Exporter..."
    ufw allow from $LOCALHOST_IP to any port 9090

    # Reload UFW
    sudo ufw reload

    echo "UFW rule added or updated and UFW reloaded."
else
    echo "UFW rule for Prometheus SSH"
fi

ssh_ufw_rule="allow from $PROXY_IP to any port 22"
existing_rule=$(ufw show added | grep "$ssh_ufw_rule")

if [ -z "$existing_rule" ]; then
    # Rule not found or has changed, add or update it
    echo "Adding or updating UFW rule for SSH..."
    ufw allow from $PROXY_IP to any port 22

    # Reload UFW
    sudo ufw reload

    echo "UFW rule added or updated and UFW reloaded."
else
    echo "UFW rule for Prometheus SSH"
fi

"""
Setting up UFW for this should look like
$ sudo ufw default deny incoming
$ sudo ufw status
Status: active

To                         Action      From
--                         ------      ----
3000                       ALLOW       172.16.1.3
22                         ALLOW       172.16.1.3
9090                       ALLOW       172.16.223.96
9090                       ALLOW       127.0.0.1
9100                       ALLOW       172.16.223.96
"""